import re


def run_tests(func, tests: dict):
    print(f'Running tests for `{func.__name__}`')
    for i, test_case in enumerate(tests.items()):
        print(f"\tTest {i}...", end=' ')
        data, expected_result = test_case
        result = func(data)
        if result != expected_result:
            print(f'FAILED: expected: `{expected_result}`, got: `{result}`')
            return False
        print('OK')
    return True


def main():
    parts = {'eyes': (':', ';', 'X', '8', '='),
             'noses': ('-', '<', '-{', '<{'),
             'mouths': ('(', ')', 'O', '|', '\\', '/', 'P'), }

    def build_smiley(n: int) -> str:
        return ''.join(part[n % len(part)] for part in parts.values())

    isu_n = 263931
    assert (smiley := build_smiley(isu_n)) == ';<{|'
    smiley_pattern = re.escape(smiley)

    def count_smileys(text: str) -> int:
        return len(re.findall(smiley_pattern, text))

    count_smileys_tests = {'no smileys': 0,
                           ' ;<{| 8<{|': 1,
                           '><{| ;<{| ': 1,
                           ';<{|;<{|': 2,
                           'qwerty;<{|42;<{|...': 2}
    run_tests(count_smileys, count_smileys_tests)


def extras():
    second_name = r'\b([А-Я][А-Яа-я]*)(?= [А-Я]\.[А-Я]\.)'

    def find_surnames(text: str) -> tuple[str]:
        return tuple(sorted(re.findall(second_name, text)))

    find_surnames_tests = {
        'И А.А.': ('И',),
        'a Б.В. A б.В. А Б.в.': (),
        'А Б.В Г,Д.': (),
        'qwerty А Б.В.42': ('А',),
        'вот они слева направо: Намджун Д.Ж., Чонгук Н.Г., '
        'Чингачгук Ч.Г., ...': ('Намджун', 'Чингачгук', 'Чонгук')}
    run_tests(find_surnames, find_surnames_tests)

    bad_record = r'\b[А-Я][А-Яа-я-]*\s([А-Я])\.(\1)\.\sP3232'

    def filter_students_list(data: tuple[str]) -> tuple[str]:
        return tuple(record for record in data
                     if not re.fullmatch(bad_record, record))

    filter_students_list_tests = {
        ('Иванов-Петров И.И. Q3232',): ('Иванов-Петров И.И. Q3232',),
        ('Иванов-Петров И.И. P32323',): ('Иванов-Петров И.И. P32323',),
        ('Иванов-Петров И.И. P3232Q',): ('Иванов-Петров И.И. P3232Q',),
        ('Иванов-Петров А.И. P3232',): ('Иванов-Петров А.И. P3232',),
        ('Иванов-Петров И.И. P3232',): ()}
    run_tests(filter_students_list, filter_students_list_tests)


if __name__ == '__main__':
    main()
    extras()
