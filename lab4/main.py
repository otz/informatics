from types import ModuleType
from typing import Optional

import impl_lib
import impl_regexp
import impl_handmade
import timeit
from pandas import Series, DataFrame, concat

import impl_lib_reverse

INPUT_PATH = 'schedule.json'
OUTPUT_PATH = 'output_{id}.yaml'
PLOT_PATH = 'plot.png'
IMPLEMENTATIONS = {
    'handmade': impl_handmade,
    'lib': impl_lib,
    'regexp': impl_regexp,
}


def convert(text: str, impl: ModuleType) -> str:
    data = impl.parse(text)
    result = impl.format(data)
    return result


def convert_file(source_path: str, sink_path_pattern: str,
                 impls: dict[str, ModuleType]) -> None:
    with open(source_path, 'r') as source_file:
        text = source_file.read()
    for impl_id, impl in impls.items():
        result = convert(text, impl)
        sink_path = sink_path_pattern.format(id=impl_id)
        with open(sink_path, 'w') as sink_file:
            sink_file.write(result)


convert_file(INPUT_PATH, OUTPUT_PATH, IMPLEMENTATIONS)


def parsing_time(text: str, parser) -> float:
    return timeit.timeit('parser(text)', number=10, globals=locals())


def formatting_time(data: object, formatter) -> float:
    return timeit.timeit('formatter(data)', number=10, globals=locals())


def conversion_time(text: str, impl: ModuleType) -> Series:
    data = impl.parse(text)
    return Series({'parsing_time': parsing_time(text, impl.parse),
                   'formatting_time': formatting_time(data, impl.format)})


def conversion_performance(source_path: str,
                           impls: dict[str, ModuleType]) -> DataFrame:
    with open(source_path, 'r') as source_file:
        text = source_file.read()
    return DataFrame({impl_id: conversion_time(text, impl)
                      for impl_id, impl in impls.items()})


def plot_performance(performance: DataFrame,
                     plot_path: Optional[str] = None) -> None:
    axes = performance.transpose().plot.bar(
        title='JSON parsing & YAML formatting implementations performance',
        ylabel='tenfold execution time, seconds', stacked=True, rot=0)
    if plot_path:
        axes.figure.savefig(plot_path)
    else:
        axes.figure.show()


performance = conversion_performance(INPUT_PATH, IMPLEMENTATIONS)
inverse_performance = conversion_performance('output_lib.yaml', {'lib_reverse': impl_lib_reverse, })
performance = concat((performance, inverse_performance), axis=1)
plot_performance(performance, PLOT_PATH)
