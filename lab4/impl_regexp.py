from common import ParseException, FormatException, YamlFormatter
from collections.abc import Mapping, Iterable, Sequence
from decimal import Decimal
from io import StringIO
import re


class JSONParser:
    @staticmethod
    def parse(text: str) -> object:
        return JSONParser(text).parse_json()

    WSS_PATTERN = re.compile(r'[ \n\r\t]*')
    STRING_PATTERN = re.compile(r'"([^\u0000-\u0019"\\]*)"')
    NUMBER_PATTERN = re.compile(r'-?(?:0|[1-9][0-9]*)(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?')
    ARRAY_PATTERN = re.compile(r'\[a]')

    def __init__(self, text: str) -> None:
        self.buffer = text

    def remained_size(self) -> int:
        return len(self.buffer)

    def is_exhausted(self) -> bool:
        return self.remained_size() == 0

    def get_current_char(self) -> str:
        return self.buffer[0]

    def get_char(self, index: int) -> str:
        return self.buffer[index]

    def expect_char(self) -> str:
        if self.is_exhausted():
            raise ParseException('Unexpected EOF: character expected')
        return self.get_current_char()

    def expect_end(self) -> None:
        if not self.is_exhausted():
            raise ParseException(f'Unexpected character: EOF expected')

    def starts_with(self, prefix: str) -> bool:
        return self.remained_size() >= len(prefix) and all(
            prefix[i] == self.get_char(i) for i in range(len(prefix)))

    def buffer_view(self, max_size: int) -> str:
        if self.remained_size() > max_size:
            return self.buffer[:max_size - 3] + '...'
        else:
            return self.buffer[:]

    def try_consume(self, target: str) -> bool:
        if self.starts_with(target):
            self.buffer = self.buffer[len(target):]
            return True
        return False

    def try_consume_any(self, targets: tuple) -> bool:
        return any(self.try_consume(target) for target in targets)

    def consume(self, target: str) -> None:
        if not self.try_consume(target):
            raise ParseException(f'Invalid data: `{target}` expected, got: `{self.buffer_view(10)}`')

    def consume_while(self, predicate) -> None:
        new_start = 0
        while new_start < len(self.buffer) and predicate(self.get_char(new_start)):
            new_start += 1
        self.buffer = self.buffer[new_start:]

    def extract_pattern(self, pattern: re.Pattern, group: int = 0) -> str:
        if match := pattern.match(self.buffer):
            self.buffer = self.buffer[match.end():]
            return match[group]
        raise ParseException()

    # def extract_with_predicate(self, predicate) -> str:
    #     saved_position = self.current_position
    #     self.consume_while(predicate)
    #     return self.extract_consumed(saved_position)

    # recursive descent parser implementation

    def parse_json(self) -> object:
        """ json ::= element """
        element = self.parse_element()
        self.expect_end()
        return element

    def parse_element(self) -> object:
        """ element ::= ws value ws """
        self.consume_ws()
        element = self.parse_value()
        self.consume_ws()
        return element

    def parse_value(self) -> object:
        """ value :: = object | array | string | number | "true" | "false" | "null" """
        char = self.expect_char()
        if char == '{':
            return self.parse_object()
        if char == '[':
            return self.parse_array()
        if char == '"':
            return self.parse_string()
        if char == 't':
            self.consume('true')
            return True
        if char == 'f':
            self.consume('false')
            return False
        if char == 'n':
            self.consume('null')
            return None
        return self.parse_number()

    def parse_object(self) -> dict:
        """ object ::= '{' ws '}' | '{' members '}'
            members ::= member | member ',' members
            member ::= ws string ws ':' element """
        object_ = dict()
        self.consume('{')
        self.consume_ws()
        if self.try_consume('}'):
            return object_
        self.parse_member(object_)
        while self.try_consume(','):
            self.parse_member(object_)
        self.consume('}')
        return object_

    def parse_member(self, object_: dict) -> None:
        """ member ::= ws string ws ':' element """
        self.consume_ws()
        key = self.parse_string()
        self.consume_ws()
        self.consume(':')
        value = self.parse_element()
        object_[key] = value

    def parse_array(self) -> list:
        """ array ::= '[' ws ']' | '[' elements ']'
            elements ::= element | element ',' elements
            element ::= ws value ws """
        array = list()
        self.consume('[')
        self.consume_ws()
        if self.try_consume(']'):
            return array
        array.append(self.parse_element())
        while self.try_consume(','):
            array.append(self.parse_element())
        self.consume(']')
        return array

    def parse_string(self) -> str:
        """ string ::= '"' characters '"'
            characters ::= "" | character characters
            character ::= '0020' . '10FFFF' - '"' - '\' | '\' escape """
        return self.extract_pattern(JSONParser.STRING_PATTERN, 1)

    def parse_number(self) -> Decimal:
        """ number ::= integer fraction exponent
            integer ::= digit | onenine digits | '-' digit | '-' onenine digits
            fraction ::= "" | '.' digits
            exponent ::= "" | 'E' sign digits | 'e' sign digits
            sign ::= "" | '+' | '-'
            digit ::= '0' | onenine
            onenine ::= '1' . '9' """
        return Decimal(self.extract_pattern(JSONParser.NUMBER_PATTERN))

    def consume_ws(self) -> None:
        """ ws ::= "" | '0020' ws | '000A' ws | '000D' ws | '0009' ws """
        if match := JSONParser.WSS_PATTERN.match(self.buffer):
            self.buffer = self.buffer[match.end():]


parse = JSONParser.parse
format = YamlFormatter.format
