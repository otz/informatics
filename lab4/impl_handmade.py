from common import ParseException, FormatException, YamlFormatter
from collections.abc import Mapping, Iterable, Sequence
from decimal import Decimal

from io import StringIO


class JSONParser:
    @staticmethod
    def parse(text: str) -> object:
        return JSONParser(text).parse_json()

    WS = (' ', '\n', '\r', '\t')
    SIGNS = ('+', '-')
    EXPONENT = ('e', 'E')

    def __init__(self, text: str) -> None:
        self.buffer = text
        self.current_position = 0
        self.end_position = len(text)

    def remaining_size(self) -> int:
        return self.end_position - self.current_position

    def is_exhausted(self) -> bool:
        return self.current_position == self.end_position

    def get_current_char(self) -> str:
        return self.buffer[self.current_position]

    def get_char(self, index: int) -> str:
        return self.buffer[self.current_position + index]

    def expect_char(self) -> str:
        if self.is_exhausted():
            raise ParseException('Unexpected EOF: character expected')
        return self.get_current_char()

    def expect_end(self) -> None:
        if not self.is_exhausted():
            raise ParseException(f'Unexpected character at {self.current_position}: EOF expected')

    def starts_with(self, prefix: str) -> bool:
        return self.remaining_size() >= len(prefix) and all(prefix[i] == self.get_char(i) for i in range(len(prefix)))

    def buffer_view(self, max_size: int) -> str:
        if self.remaining_size() > max_size:
            return self.buffer[self.current_position:max_size - 3] + '...'
        else:
            return self.buffer[self.current_position:self.end_position]

    def try_consume(self, target: str) -> bool:
        if self.starts_with(target):
            self.current_position += len(target)
            return True
        return False

    def try_consume_any(self, targets: tuple) -> bool:
        return any(self.try_consume(target) for target in targets)

    def consume(self, target: str) -> None:
        if not self.try_consume(target):
            raise ParseException(f'Invalid data starting form character {self.current_position}:'
                                 f' `{target}` expected, got: `{self.buffer_view(10)}`')

    def consume_while(self, predicate) -> None:
        new_position = self.current_position
        while new_position < self.end_position and predicate(self.buffer[new_position]):
            new_position += 1
        self.current_position = new_position

    def extract_consumed(self, saved_position: int) -> str:
        return self.buffer[saved_position:self.current_position]

    def extract_with_predicate(self, predicate) -> str:
        saved_position = self.current_position
        self.consume_while(predicate)
        return self.extract_consumed(saved_position)

    # recursive descent parser implementation

    def parse_json(self) -> object:
        """ json ::= element """
        element = self.parse_element()
        self.expect_end()
        return element

    def parse_element(self) -> object:
        """ element ::= ws value ws """
        self.consume_ws()
        element = self.parse_value()
        self.consume_ws()
        return element

    def parse_value(self) -> object:
        """ value :: = object | array | string | number | "true" | "false" | "null" """
        char = self.expect_char()
        if char == '{':
            return self.parse_object()
        if char == '[':
            return self.parse_array()
        if char == '"':
            return self.parse_string()
        if char == 't':
            self.consume('true')
            return True
        if char == 'f':
            self.consume('false')
            return False
        if char == 'n':
            self.consume('null')
            return None
        return self.parse_number()

    def parse_object(self) -> dict:
        """ object ::= '{' ws '}' | '{' members '}'
            members ::= member | member ',' members
            member ::= ws string ws ':' element """
        object_ = dict()
        self.consume('{')
        self.consume_ws()
        if self.try_consume('}'):
            return object_
        self.parse_member(object_)
        while self.try_consume(','):
            self.parse_member(object_)
        self.consume('}')
        return object_

    def parse_member(self, object_: dict) -> None:
        """ member ::= ws string ws ':' element """
        self.consume_ws()
        key = self.parse_string()
        self.consume_ws()
        self.consume(':')
        value = self.parse_element()
        object_[key] = value

    def parse_array(self) -> list:
        """ array ::= '[' ws ']' | '[' elements ']'
            elements ::= element | element ',' elements
            element ::= ws value ws """
        array = list()
        self.consume('[')
        self.consume_ws()
        if self.try_consume(']'):
            return array
        array.append(self.parse_element())
        while self.try_consume(','):
            array.append(self.parse_element())
        self.consume(']')
        return array

    def parse_string(self) -> str:
        """ string ::= '"' characters '"'
            characters ::= "" | character characters """
        self.consume('"')
        string = self.extract_with_predicate(JSONParser.is_character)
        self.consume('"')
        return string

    @staticmethod
    def is_character(char: str) -> bool:
        """ character ::= '0020' . '10FFFF' - '"' - '\' | '\' escape """
        return ('\u0020' <= char) and (char != '\'') and (char != '"')

    def parse_number(self) -> Decimal:
        """ number ::= integer fraction exponent
            integer ::= digit | onenine digits | '-' digit | '-' onenine digits
            fraction ::= "" | '.' digits
            exponent ::= "" | 'E' sign digits | 'e' sign digits
            sign ::= "" | '+' | '-' """
        number_start = self.current_position
        self.try_consume('-')
        if not self.try_consume('0'):
            self.consume_digits()
        if self.try_consume('.'):
            self.consume_digits()
        if self.try_consume_any(JSONParser.EXPONENT):
            self.try_consume_any(JSONParser.SIGNS)
            self.consume_digits()
        return Decimal(self.extract_consumed(number_start))

    def consume_digits(self) -> None:
        self.consume_while(JSONParser.is_digit)

    @staticmethod
    def is_digit(char: str) -> bool:
        """ digit ::= '0' | onenine
            onenine ::= '1' . '9' """
        return '0' <= char <= '9'

    def consume_ws(self) -> None:
        self.consume_while(JSONParser.is_ws)

    @staticmethod
    def is_ws(char: str) -> bool:
        """ ws ::= "" | '0020' ws | '000A' ws | '000D' ws | '0009' ws """
        return char in JSONParser.WS


parse = JSONParser.parse
format = YamlFormatter.format
