import functools
import yaml
import json

parse = json.JSONDecoder().decode
format = functools.partial(yaml.dump, allow_unicode=True)
