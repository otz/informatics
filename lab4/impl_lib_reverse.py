import functools
import yaml
import json

parse = functools.partial(yaml.load, Loader=yaml.Loader)
format = json.JSONEncoder().encode
