from collections.abc import Mapping, Sequence
from decimal import Decimal
from io import StringIO


class ParseException(Exception):
    def __init__(self, message: str):
        self.message = message

    def __str__(self):
        return self.message


class FormatException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class YamlFormatter:
    @staticmethod
    def format(data: object) -> str:
        formatter = YamlFormatter()
        formatter.format_node(data)
        return formatter.collect()

    SINGLETON_SCALARS_FORMATS = {None: 'null', True: 'true', False: 'false'}

    def __init__(self) -> None:
        self.buffer = StringIO()
        self.nesting_level = 0
        self.is_padding_required = False

    def write_raw(self, data: str) -> None:
        self.buffer.write(data)

    def newline(self) -> None:
        self.write_raw('\n')
        self.is_padding_required = True

    def write_padding(self) -> None:
        for _ in range(self.nesting_level):
            self.write_raw('  ')

    def write(self, *data: str) -> None:
        if self.is_padding_required:
            self.write_padding()
            self.is_padding_required = False
        for item in data:
            self.write_raw(item)

    def enter_block(self) -> None:
        self.nesting_level += 1

    def leave_block(self) -> None:
        self.nesting_level -= 1

    def collect(self) -> str:
        return self.buffer.getvalue()

    def __del__(self) -> None:
        self.buffer.close()

    def format_node(self, node: object) -> None:
        if YamlFormatter.is_mapping_node(node):
            return self.format_mapping(node)
        if YamlFormatter.is_sequence_node(node):
            return self.format_sequence(node)
        return self.format_scalar(node)

    def format_sequence(self, node: Sequence) -> None:
        for value in node:
            self.write('- ')
            self.enter_block()
            self.format_node(value)
            self.leave_block()

    @staticmethod
    def is_sequence_node(node: object) -> bool:
        return (not isinstance(node, str)) and isinstance(node, Sequence)

    def format_mapping(self, node: Mapping) -> None:
        for key, value in sorted(node.items()):
            self.write_scalar(key)
            self.write(':')
            if YamlFormatter.is_mapping_node(value):
                self.newline()
                self.enter_block()
                self.format_mapping(value)
                self.leave_block()
            elif YamlFormatter.is_sequence_node(value):
                self.newline()
                self.format_sequence(value)
            else:
                self.write(' ')
                self.format_scalar(value)

    @staticmethod
    def is_mapping_node(node: object) -> bool:
        return isinstance(node, Mapping)

    def format_scalar(self, node: object) -> None:
        self.write_scalar(node)
        self.newline()

    def write_scalar(self, node: object) -> None:
        if isinstance(node, str):
            if ':' in node:
                self.write('\'', node, '\'')
            else:
                self.write(node)
        elif isinstance(node, Decimal) | isinstance(node, int) | isinstance(node, float):
            self.write(str(node))
        elif node in YamlFormatter.SINGLETON_SCALARS_FORMATS:
            self.write(YamlFormatter.SINGLETON_SCALARS_FORMATS.get(node))
        else:
            raise FormatException(f'Scalars of type {type(node)} are not supported')
