\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{quant-article}[2021/12/02 The Quant Journal article]

\LoadClass[9pt, a4paper, twocolumn, twoside]{extarticle}

\usepackage[utf8]{inputenc}
\usepackage[russian]{babel}

\usepackage[textwidth=480pt, textheight=670pt, headsep=10pt]{geometry}
\setlength\columnsep{20pt}

\usepackage{fancyhdr}
\pagestyle{fancy}

\usepackage{graphicx}
\usepackage[format=plain,
            font=it]{caption}

\usepackage{xcolor}
\newcommand{\customsection}[1]{\section*{\centering #1}}
